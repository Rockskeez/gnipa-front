// import { AUTH_TOKEN } from '@/lib/constants'

export const state = () => ({
  SONGS: [],
  CURRENT_SONG: null,
  CURRENT_SONG_INFO: {},
})

export const getters = {
  getSongs(state) {
    return state.SONGS
  }
}

export const mutations = {
  setSongs(state, songs ) {
    state.SONGS = songs
  },
  setSongInfo(state, songInfo) {
    state.CURRENT_SONG_INFO = songInfo
  },
  setSong(state, song) {
    state.CURRENT_SONG = song
  },
}

export const actions = {
  async getSongs({ commit }, params) {
    const resp = await this.$api.getSongs(params)
    commit('setSongs', resp.data)
    return resp
  },
  async setSongsByAlbum({ commit }, id){
    const resp = await this.$api.getAlbumById(id)
    commit('setSongs', resp.data)
    return resp
  },
  async getSongInfo({ commit }, id) {
    const resp = await this.$api.getSongInfo(id)
    commit('setSongInfo', resp.data)
    return resp
  },
  async getSong({ commit }, id) {
    const resp = await this.$api.getSong(id)
    commit('setSong', resp.data)
    return resp
  },
}
