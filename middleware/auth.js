import { AUTH_TOKEN } from '@/lib/constants'

export default function ({ app, redirect }) {
  const isLoggedIn = app.$cookies.get(AUTH_TOKEN)

  if (!isLoggedIn) redirect('/')
}
