// import { AUTH_TOKEN } from '@/lib/constants'

export const state = () => ({
  ALBUMS: [],
  CURRENT_ALBUM: null
})

export const getters = {
  getAlbums(state) {
    return state.ALBUMS
  }
}

export const mutations = {
  setAlbums(state, albums ) {
    state.ALBUMS = albums
  },
  setNumberOfAlbum(state, id) {
    state.CURRENT_ALBUM = id
  }
}

export const actions = {
  async getAlbums({ commit }, params) {
    const resp = await this.$api.getAlbums(params)
    commit('setAlbums', resp.data)
    return resp
  },
}
