// import { AUTH_TOKEN } from '@/lib/constants'

export const state = () => ({
  USERS: [],
})

export const getters = {
  getUsers(state) {
    return state.USERS
  }
}

export const mutations = {
  setUsers(state, users ) {
    state.USERS = users
  },
}

export const actions = {
  async getUsers({ commit }, nickname) {
    const resp = await this.$api.searchUsers(nickname)
    commit('setUsers', resp.data)
    return resp
  },
}
