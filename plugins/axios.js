import { createApi } from '~/lib/api'
import { AUTH_TOKEN } from '~/lib/constants'

export default function ({ $axios, $cacheFetch, redirect, error: nuxtError, store, $cookies }, inject) {

  $axios.onRequest((config) => {
    config.headers = {
    'Content-Type': 'application/json',
    'Accept': '*/*',
    [`${AUTH_TOKEN}`]: $cookies.get(AUTH_TOKEN) || '',
    }
  })

  $axios.onError(error => {
    if (error.response.status === 500) {
      redirect('/maintenance')
    }
  })

  const API = createApi(
    $axios,
    $cacheFetch,
  )

  inject('api', API)
}


