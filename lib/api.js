import { stringify } from 'querystring'

const headersForMp3 = {
  'Content-Type': 'audio/mpeg'
}

export const createApi = (axios, cache) => ({

  // profile

  getProfileInfo () {
    return axios.get('/users/info')
    .catch(err => {
      return err.response
    })
  },

  getProfileSongs (id) {
    return axios.get(`/songs/user/${id}`)
    .catch(err => {
      return err.response
    })
  },

  updateSong(id, title, songTags) {
    return axios.put(`/songs/${id}`, {
      title,
      songTags
    })
    .catch(err => {
      return err.response
    })
  },

  removeSong(id) {
    return axios.delete(`/songs/${id}`)
    .catch(err => {
      return err.response
    })
  },

  getProfileAlbums (id) {
    return axios.get(`/albums/user/${id}`)
    .catch(err => {
      return err.response
    })
  },

  updateAlbum(id, title) {
    return axios.put(`/albums/${id}`, {
      title
    })
    .catch(err => {
      return err.response
    })
  },

  removeAlbum(id) {
    return axios.delete(`/albums/${id}`)
    .catch(err => {
      return err.response
    })
  },

  // users

  searchUsers(nickname){
    return axios.get(`/users/search?${stringify(nickname)}`)
    .catch(err => {
      return err.response
    })
  },

  removeUser(id) {
    return axios.delete(`/users/${id}`)
    .catch(err => {
      return err.response
    })
  },

  // security
  subscriptionSaveEmail () {
    return axios.get('/security/role')
    .catch(err => {
      return err.response
    })
  },

  register (params) {
    return axios.post('/security/register', params)
    .catch(err => {
      return err.response
    })
  },

  login (params) {
    return axios.post('/security/auth', params)
    .catch(err => {
      return err.response
    })
  },
  //

  // songs
  getSongs (params) {
    return axios.get(`/songs/search?${stringify(params)}`)
    .catch(err => {
      return err.response
    })
  },

  getSongInfo (id) {
    return axios.get(`/songs/${id}`)
    .catch(err => {
      return err.response
    })
  },

  getSong (id) {
    return axios.get(`/songs/load/${id}/file`, { headers: headersForMp3 })
    .catch(err => {
      return err.response
    })
  },

  addSong (params) {
    return axios.post('/songs/', params)
    .catch(err => {
      return err.response
    })
  },

  // albums
  getAlbums (params) {
    return axios.get(`/albums/search?${stringify(params)}`)
    .catch(err => {
      return err.response
    })
  },

  getAlbumById (id) {
    return axios.get(`/albums/${id}/songs`)
    .catch(err => {
      return err.response
    })
  },

  addSongToAlbum(albumId, songId) {
    return axios.post(`/albums/${albumId}/songs/${songId}`)
    .catch(err => {
      return err.response
    })
  },

  removeSongFromAlbum(albumId, songId) {
    return axios.delete(`/albums/${albumId}/songs/${songId}`)
    .catch(err => {
      return err.response
    })
  },

  addAlbum(params) {
    return axios.post('/albums/', params)
    .catch(err => {
      return err.response
    })
  }
})
