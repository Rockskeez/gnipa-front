import { AUTH_TOKEN } from '@/lib/constants'

export const state = () => ({
  USER_ROLE: '',
  USER_INFO: null,
  USER_SONGS: [],
  USER_ALBUMS: [],
})

export const getters = {
  getUserRole(state) {
    return state.USER_ROLE
  }
}

export const mutations = {
  setUserRole(state, role ) {
    state.USER_ROLE = role
  },

  setUserInfo(state, info) {
    state.USER_INFO = info
  },

  setUserSongs(state, songs) {
    state.USER_SONGS = songs
  },

  setUserAlbums(state, albums) {
    state.USER_ALBUMS = albums
  }
}

export const actions = {
  async getUserRole({ commit }) {
    const { data, status} = await this.$api.subscriptionSaveEmail()
    if (status !== 200) {
      alert(data.error)
    }
    commit('setUserRole', data)
  },

  async getUserInfo({ commit }) {
    const { data, status} = await this.$api.getProfileInfo()
    if (status !== 200) {
      alert(data.error)
    }
    commit('setUserInfo', data)
    return data
  },

  async getProfileSongs({ commit }, id) {
    console.log(id)
    const { data, status} = await this.$api.getProfileSongs(id)
    if (status !== 200) {
      alert(data.error)
    }
    commit('setUserSongs', data)
  },

  async getProfileAlbums({ commit }, id) {
    console.log(id)
    const { data, status} = await this.$api.getProfileAlbums(id)
    if (status !== 200) {
      alert(data.error)
    }
    commit('setUserAlbums', data)
  },

  async loginUser({ dispatch }, params) {
      const { data, status } = await this.$api.login(params)

      if (status !== 200) {
        alert(data.error)
        return
      }

      this.$cookies.set(AUTH_TOKEN, data)

      dispatch('getUserRole')

      this.$router.push({path: '/'})
  },

  logout ({dispatch}) {
    this.$cookies.remove(AUTH_TOKEN)

    dispatch('getUserRole')
    this.$router.push({path: '/'})
  }
}
